 ########################### \
  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐   \
   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐  \
  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘  \
 ########################### \

 # https://gitlab.com/Asperatus/designtab

ERROR_FLAGS = -Wall -pedantic -Werror
LINKING_FLAGS = 
COMPILE_FLAGS = -std=c++11
EXECUTION_FLAGS = 
SFML_FLAGS = -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

LINUX_COMPILER = g++
WIN32_COMPILER = i686-w64-mingw32-g++
WIN64_COMPILER = x86_64-w64-mingw32-g++

BIN = bin
OBJ = obj
SRC = src
NAME = designtab

SOURCES = $(shell find $(SRC) -wholename "*.cpp")
OBJECTS_LINUX = $(patsubst $(SRC)/%.cpp, $(OBJ)/linux/%.o, $(SOURCES))
OBJECTS_WIN32 = $(patsubst $(SRC)/%.cpp, $(OBJ)/win32/%.o, $(SOURCES))
OBJECTS_WIN64 = $(patsubst $(SRC)/%.cpp, $(OBJ)/win64/%.o, $(SOURCES))

MINGW_X32_PATH = /usr/i686-w64-mingw32
MINGW_X64_PATH = /usr/x86_64-w64-mingw32


### ---  LINUX

run: linux
	@echo "Launching $(NAME)..."
	@bin/linux/$(NAME) $(EXECUTION_FLAGS) 

linux: $(BIN)/linux $(OBJ)/linux $(OBJECTS_LINUX)
	@echo "Linking $(NAME) (for Linux)..."
	@$(LINUX_COMPILER) $(OBJECTS_LINUX) -o $(BIN)/linux/$(NAME) $(LINKING_FLAGS) $(SFML_FLAGS) $(ERROR_FLAGS)

$(BIN)/linux:
	@echo "Creating $(BIN)/linux folder"
	@mkdir -p $(BIN)/linux

$(OBJ)/linux:
	@echo "Creating $(OBJ)/linux folders"
	@mkdir -p $(OBJ)/linux
	@find $(SRC) -mindepth 1 -type d | sed 's/^....//' | while read dir; do mkdir $(OBJ)/linux/$$dir; done

$(OBJECTS_LINUX): $(OBJ)/linux/%.o : $(SRC)/%.cpp
	@echo "Compiling $<..."
	@$(LINUX_COMPILER) -c $< -o $@ $(COMPILE_FLAGS) $(ERROR_FLAGS)


### ---  WINDOWS

windows: $(BIN)/windows $(OBJ)/windows $(OBJECTS_WIN32) $(OBJECTS_WIN64)
	@echo "Linking $(NAME) (for Windows)..."
	@$(WIN32_COMPILER) $(OBJECTS_WIN32) -o $(BIN)/win32/$(NAME).exe -LSFML/SJLJ/lib $(LINKING_FLAGS) $(SFML_FLAGS) $(ERROR_FLAGS)
	@$(WIN64_COMPILER) $(OBJECTS_WIN64) -o $(BIN)/win64/$(NAME).exe -LSFML/SEH/lib $(LINKING_FLAGS) $(SFML_FLAGS) $(ERROR_FLAGS)
	@echo "Copying DLLs..."
	@cp SFML/SJLJ/bin/* $(BIN)/win32
	@cp SFML/SEH/bin/* $(BIN)/win64
	@cp $(MINGW_X32_PATH)/bin/libstdc++-6.dll $(MINGW_X32_PATH)/bin/libgcc_s_sjlj-1.dll $(MINGW_X32_PATH)/bin/libwinpthread-1.dll $(BIN)/win32
	@cp $(MINGW_X64_PATH)/bin/libstdc++-6.dll $(MINGW_X64_PATH)/bin/libgcc_s_seh-1.dll $(MINGW_X64_PATH)/bin/libwinpthread-1.dll $(BIN)/win64

$(BIN)/windows:
	@echo "Creating $(BIN)/win64 and $(BIN)/win32 folders"
	@mkdir -p $(BIN)/win64 $(BIN)/win32

$(OBJECTS_WIN32): $(OBJ)/win32/%.o : $(SRC)/%.cpp
	@echo "Compiling $< (for Windows x32)..."
	@$(WIN32_COMPILER) -c $< -o $@ -ISFML/SJLJ/include $(COMPILE_FLAGS) $(ERROR_FLAGS)

$(OBJECTS_WIN64): $(OBJ)/win64/%.o : $(SRC)/%.cpp
	@echo "Compiling $< (for Windows x64)..."
	@$(WIN64_COMPILER) -c $< -o $@ -ISFML/SEH/include $(COMPILE_FLAGS) $(ERROR_FLAGS)

$(OBJ)/windows:
	@echo "Creating $(OBJ)/win64 and $(OBJ)/win32 folders"
	@mkdir -p $(OBJ)/win32 $(OBJ)/win64
	@find $(SRC) -mindepth 1 -type d | sed 's/^....//' | while read dir; do mkdir $(OBJ)/win32/$$dir $(OBJ)/win64/$$dir; done


### ---  COMMON

all: linux windows

clean:
	@echo "Cleaning files..."
	@rm -Rf $(BIN) $(OBJ)
