# designtab

**designtab** is a video editing software based on the *.osb* file format.

## Installation

You will find, in the [Releases](https://gitlab.com/Asperatus/designtab/releases) page, the latest releases of the program.

If you want to have access to the production release, please see the section named *Contribute*.

## Contribute

Any help on designtab is greatly appreciated.

The repository is shipped with a *makefile* that will export the program in *Windows* and/or *Linux*.

**Warning**: the compilation process has only been adapted to be used on Linux environments. We'd love to hear possible other ways to contribute to the program!

The following programs are needed in order to compile the program:

- To export for Linux: `g++`(available out-of-the-box on Debian-based distributions (Ubuntu, Mint, ...) and on various other distributions, available in the group `base-devel` on Arch-based distributions)

- To export for Windows: `mingw-w64` (as the package `mingw-w64` on Debian-based distributions or in [the AUR](https://aur.archlinux.org/packages/mingw-w64-gcc/) on Arch-based distributions)

- `make` (available out-of-the-box on Debian-based distributions (Ubuntu, Mint, ...) and on various other distributions, available in the group `base-devel` on Arch-based distributions)

- The latest version of SFML (as the package `libsfml-dev` on Debian-based distributions or the package `sfml` on Arch-based distributions)

Here are the available commands (to be launched in the root directory of the project):

- `make` or `make run` will compile the program for Linux and will run it.

- `make all` will compile the program for both Linux and Windows.

- `make linux` will compile the program for Linux.

- `make windows` will compile the program for Windows (see the category "Prerequisites for Windows").

## Prerequisites for Windows

You will need, in order to export for Windows, to edit the file named `makefile`¹ and download the Windows version of SFML².

1- In the `makefile`:

- Specify the correct *MingW* commands in `WIN32_COMPILER` and `WIN64_COMPILER`.

- Check that the directories specified in `MINGW_X32_PATH` and `MINGW_X64_PATH` are correct (we use files located in the `bin` folder).

2- You can download SFML's *Windows* versions on [their website](https://www.sfml-dev.org/download.php) (we need SJLJ for 32 bits computer and SEH for 64 bits computer). Launch `mkdir -p SFML/SJLJ SFML/SEH` in the root directory of the project and copy, in the folder `SFML/SJLJ`, the corresponding version of SFML, and in the folder `SFML/SEH` the corresponding version of SFML too.

## License

This program is shipped with a GPL3 license.

## Get in touch

If you want to tell about an issue, we recommend you head over to the [Issues](https://gitlab.com/Asperatus/designtab/issues) page.

We'd also love to see you on our [Discord server](https://discord.gg/aqKU2Qz)!
