/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#ifndef __BUTTON_HPP
#define __BUTTON_HPP

#include <SFML/Graphics.hpp>
#include "../Utils/Origin.hpp"
#include "../Window.hpp"
#include <iostream>

class Button
{
	public:
		void handleInputs(sf::Event event)
		{
			if (is_pressed)
			{
				if (event.type == sf::Event::MouseButtonReleased)
				{
					onRelease();
					is_pressed = false;
					is_clicked = true;
				}
		
				if (event.type == sf::Event::MouseMoved)
				{
					int mouse_x = event.mouseMove.x;
					int mouse_y = event.mouseMove.y;
		
					if (!mouseOnButton(mouse_x, mouse_y))
					{
						onRelease();
						is_pressed = false;
					}
				}
			}
		
			if (event.type == sf::Event::MouseButtonPressed)
			{
				int mouse_x = event.mouseButton.x;
				int mouse_y = event.mouseButton.y;
		
				if (mouseOnButton(mouse_x, mouse_y))
				{
					onPress();
					is_pressed = true;
				}
			}
		}

		virtual void move(int x, int y) = 0;
		virtual void draw(const float dt) = 0;
		bool is_clicked = false;

	protected:
		Window* window;
		int x;
		int y;
		unsigned int width;
		unsigned int height;
		Origin origin;
		bool is_pressed = false;

		virtual void onPress() = 0;
		virtual void onRelease() = 0;

		virtual inline bool mouseOnButton(int mouse_x, int mouse_y)
		{
			return mouse_x >= x && mouse_y >= y &&
				   mouse_x <= x + static_cast<int>(width) && mouse_y <= y + static_cast<int>(height);
		}

};

#endif // __BUTTON_HPP
