/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#include "TextButton.hpp"
#include <algorithm>

TextButton::TextButton() {}

TextButton::TextButton(Window* window, const char* text, sf::Font* font, Origin origin, int x, int y, float buttonWidth, float buttonHeight)
{
	this->window = window;
	this->origin = origin;

	int margin_left_right = 10;
	int margin_top_bottom = 10;

	this->text = sf::Text(text, *font);
	this->text.setCharacterSize(14);
	this->text.setFillColor(sf::Color::White);

	sf::FloatRect textRect = this->text.getLocalBounds();

	float text_width = textRect.width;
	float text_height = textRect.height;

	this->text.setOrigin(static_cast<int>(textRect.left + text_width  / 2.0f),
	                     static_cast<int>(textRect.top  + text_height / 2.0f));

	this->width  = std::max(buttonWidth,  text_width  + 2 * margin_left_right);
	this->height = std::max(buttonHeight, text_height + 2 * margin_top_bottom);

	this->background = sf::RectangleShape(sf::Vector2f(this->width, this->height));
	this->background.setFillColor(sf::Color(23, 25, 25, 255));

	switch(origin)
	{
		case Origin::TopLeft:
			this->background.setOrigin(0, 0);
			break;
		case Origin::TopCentre:
			this->background.setOrigin(static_cast<int>(this->width / 2.0f), 0);
			break;
		case Origin::TopRight:
			this->background.setOrigin(static_cast<int>(this->width), 0);
			break;
		case Origin::CentreLeft:
			this->background.setOrigin(0, static_cast<int>(this->height / 2.0f));
			break;
		case Origin::Centre:
			this->background.setOrigin(static_cast<int>(this->width / 2.0f), static_cast<int>(this->height / 2.0f));
			break;
		case Origin::CentreRight:
			this->background.setOrigin(static_cast<int>(this->width), static_cast<int>(this->height / 2.0f));
			break;
		case Origin::BottomLeft:
			this->background.setOrigin(0, static_cast<int>(this->height));
			break;
		case Origin::BottomCentre:
			this->background.setOrigin(static_cast<int>(this->width / 2.0f), static_cast<int>(this->height));
			break;
		case Origin::BottomRight:
			this->background.setOrigin(static_cast<int>(this->width), static_cast<int>(this->height));
			break;
	}

	move(x, y);
}

void TextButton::move(int x, int y)
{
	this->background.setPosition(x, y);

	switch(this->origin)
	{
		case Origin::TopLeft:
			this->text.setPosition(x + this->width / 2.0f, y + this->height / 2.0f);
			this->x = x;
			this->y = y;
			break;
		case Origin::TopCentre:
			this->text.setPosition(x, y + this->height / 2.0f);
			this->x = x - this->width / 2.0f;
			this->y = y;
			break;
		case Origin::TopRight:
			this->text.setPosition(x - this->width / 2.0f, y + this->height / 2.0f);
			this->x = x - this->width;
			this->y = y;
			break;
		case Origin::CentreLeft:
			this->text.setPosition(x + this->width / 2.0f, y);
			this->x = x;
			this->y = y - this->height / 2.0f;
			break;
		case Origin::Centre:
			this->text.setPosition(x, y);
			this->x = x - this->width / 2.0f;
			this->y = y - this->height / 2.0f;
			break;
		case Origin::CentreRight:
			this->text.setPosition(x - this->width / 2.0f, y);
			this->x = x - this->width;
			this->y = y - this->height / 2.0f;
			break;
		case Origin::BottomLeft:
			this->text.setPosition(x + this->width / 2.0f, y - this->height / 2.0f);
			this->x = x;
			this->y = y - this->height;
			break;
		case Origin::BottomCentre:
			this->text.setPosition(x, y - this->height / 2.0f);
			this->x = x - this->width / 2.0f;
			this->y = y - this->height;
			break;
		case Origin::BottomRight:
			this->text.setPosition(x - this->width / 2.0f, y - this->height / 2.0f);
			this->x = x - this->width;
			this->y = y - this->height;
			break;
	}
}

void TextButton::onPress()
{
	this->background.setFillColor(sf::Color::Black);
}

void TextButton::onRelease()
{
	this->background.setFillColor(sf::Color(23, 25, 25, 255));
}

void TextButton::draw(const float dt)
{
	window->renderWindow.draw(this->background);
	window->renderWindow.draw(this->text);

	if (this->is_clicked)
		is_clicked = false;
}
