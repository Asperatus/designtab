/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#ifndef __TEXTBUTTON_HPP
#define __TEXTBUTTON_HPP

#include "Button.hpp"

class TextButton : public Button
{
	public:
		TextButton();
		TextButton(Window* window, const char* text, sf::Font* font, Origin origin, int x, int y, float buttonWidth = 0, float buttonHeight = 0);

		virtual void move(int x, int y);
		virtual void draw(const float dt);

	protected:
		sf::RectangleShape background;
		sf::Text text;

		virtual void onPress();
		virtual void onRelease();
};

#endif // __TEXTBUTTON_HPP
