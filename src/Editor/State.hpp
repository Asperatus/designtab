/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#ifndef __STATE_HPP
#define __STATE_HPP

#include <SFML/Graphics.hpp>
#include "Window.hpp"

class State
{
	public:
		virtual void onStart() = 0;
		virtual void onClose() = 0;

		virtual void onScreenResize(int windowWidth, int windowHeight) = 0;

		virtual void handleEvents(sf::Event event) = 0;
		virtual void draw  (const float dt) = 0;
		virtual void update(const float dt) = 0;
	
	protected:
		Window* window;
};

#endif // __STATE_HPP
