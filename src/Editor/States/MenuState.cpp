/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#include "MenuState.hpp"
#include <iostream>

MenuState::MenuState(Window* window)
{
	this->window = window;
}

void MenuState::onStart()
{
	this->text_button = TextButton(window, "Button", &this->window->font, Origin::Centre, this->window->getWidth() / 2, this->window->getHeight() / 2);
}

void MenuState::onClose() {}

void MenuState::onScreenResize(int windowWidth, int windowHeight) { this->text_button.move(windowWidth / 2, windowHeight / 2); }

void MenuState::draw  (const float dt) { this->text_button.draw(dt); }
void MenuState::update(const float dt) { if (this->text_button.is_clicked) printf("Button click!\n"); }
void MenuState::handleEvents(sf::Event event) { this->text_button.handleInputs(event); }
