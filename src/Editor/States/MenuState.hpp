/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#ifndef __MENUSTATE_HPP
#define __MENUSTATE_HPP

#include "../State.hpp"
#include "../UI/TextButton.hpp"

class MenuState : public State
{
	public:
		MenuState(Window* window);

		virtual void onStart();
		virtual void onClose();

		virtual void onScreenResize(int windowWidth, int windowHeight);

		virtual void draw  (const float dt);
		virtual void update(const float dt);
		virtual void handleEvents(sf::Event event);

	protected:
		TextButton text_button;
};

#endif // __MENUSTATE_HPP
