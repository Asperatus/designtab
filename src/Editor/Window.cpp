/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#include <SFML/Graphics.hpp>
#include "Window.hpp"
#include "State.hpp"
#include "States/MenuState.hpp"

Window::Window() { }
Window::~Window() { }


/* Window management */

void Window::init()
{
	windowWidth = 1280;
	windowHeight = 720;

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	renderWindow.create(sf::VideoMode(windowWidth, windowHeight), "designtab", sf::Style::Default, settings);
	renderWindow.setFramerateLimit(60);

	font.loadFromFile("res/nimbus.otf");

	pushState(new MenuState(this));
}

void Window::update()
{
	sf::Time elapsed = clock.restart();
	float dt = elapsed.asSeconds();

	State* currentState = getCurrentState();

	if (currentState == nullptr)
	{
		renderWindow.close();
		return;
	}

	sf::Event event;
	while (renderWindow.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			renderWindow.close();
		
		if (event.type == sf::Event::Resized)
		{
			sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
			renderWindow.setView(sf::View(visibleArea));
			windowWidth = event.size.width;
			windowHeight = event.size.height;

			currentState->onScreenResize(windowWidth, windowHeight);
		}

		currentState->handleEvents(event);
	}

	currentState->update(dt);

	renderWindow.clear(sf::Color(46, 49, 49, 255));

	currentState->draw(dt);

	renderWindow.display();
}

void Window::destroy()
{
	while (!states.empty())
		popState();
}

bool Window::isOpen() const { return renderWindow.isOpen(); }
int Window::getWidth() { return windowWidth; }
int Window::getHeight() { return windowHeight; }


/* States management */

void Window::pushState(State* state)
{
	state->onStart();
	states.push_back(state);
}

void Window::popState()
{
	states.back()->onClose();
	states.pop_back();

	if (!states.empty())
		getCurrentState()->onScreenResize(getWidth(), getHeight());
}

State* Window::getCurrentState()
{
	if (states.empty())
		return nullptr;
	else
		return states.back();
}
