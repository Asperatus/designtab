/***
  *  ┌┬┐┌─┐┌─┐┬┌─┐┌┐┌┌┬┐┌─┐┌┐ 
  *   ││├┤ └─┐││ ┬│││ │ ├─┤├┴┐
  *  ─┴┘└─┘└─┘┴└─┘┘└┘ ┴ ┴ ┴└─┘
  *  https://gitlab.com/Asperatus/designtab
 ***/

#ifndef __WINDOW_H
#define __WINDOW_H

#include <SFML/Graphics.hpp>
#include <vector>

class State;

class Window
{
	public:
		sf::RenderWindow renderWindow;

		Window();
		~Window();

		/* Window management */
		void init();
		void update();
		void destroy();

		bool isOpen() const;
		int getWidth();
		int getHeight();

		/* States management */
		void pushState(State* state);
		void popState();

		State* getCurrentState();
		sf::Font font;

	private:
		sf::Clock clock;
		unsigned int windowWidth;
		unsigned int windowHeight;
		std::vector<State*> states;
};

#endif // __WINDOW_H
